angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$rootScope,$ionicHistory,$ionicSideMenuDelegate) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
	/* JCI */
	
	$scope.menuCategories = [];
  
	$rootScope.$watch('CategoryArray', function(newValue, oldValue) {
      $scope.menuCategories = $rootScope.CategoryArray.categories;
    });  
	
	
	$scope.navigateHome = function()
	{
		window.location ="#/app/main";
	}

	$scope.navigatePage = function(index)
	{
		window.location ="#/app/jci_details/"+index;	
	}

	$scope.navigatePhone = function()
	{
		window.location ="#/app/jci_phonebook";		
	}

	
	$scope.navigateMessages = function()
	{

		window.location ="#/app/jci_pushmessages";	
	}


	 $scope.toggleRightSideMenu = function() 
	 {
    	$ionicSideMenuDelegate.toggleRight();
  	 };
	 
	 $scope.GoBack = function() 
	 {
    	$ionicHistory.goBack();
    	//$rootScope.notifyIonicGoingBack();
  	 };	 
	 
	
	/* JCI END */
  
	$scope.checkLoggedIn = function()
	{
		if (window.localStorage.employeeid)
			$scope.userLoggedIn = true;
		else
			$scope.userLoggedIn = false;
	}
	
	$scope.checkLoggedIn();

	$scope.EmployeeLogin = function()
	{
		if (!window.localStorage.employeeid)
			window.location.href = "#/app/employeelogin";
		else
			window.location.href = "#/app/employeemain";
	}
	
	$scope.logOut = function()
	{
		window.localStorage.employeeid = '';
		$rootScope.Employee  = 0;
		$scope.checkLoggedIn();
		$ionicSideMenuDelegate.toggleRight();
		
		$ionicHistory.nextViewOptions({
				disableAnimate: true,
				expire: 300,
				disableBack: true
		 });
				 
		window.location.href = "#/app/main";
	}
  
})

.controller('MainCtrl', function($scope, $stateParams,$rootScope) {
	
		
	  $scope.$on('$ionicView.enter', function(e) {
		  $rootScope.Employee  = 0;
	  });
})


.controller('AboutCtrl', function($scope, $stateParams,$rootScope) {
	
  $scope.AboutData = $rootScope.MainDataArray.about;
  console.log($scope.AboutData);

})

.controller('PhonebookCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate) {
	
  $scope.PhonebookContacts = $rootScope.MainDataArray.phonebook;
  console.log($scope.PhonebookContacts);

  
	$scope.DialPhone = function(phone)
	{
		window.open('tel:' + phone, '_system');
	}
	
	$scope.BlurContacts = function()
	{
		$ionicScrollDelegate.scrollTop();
	}

	
	$scope.byContactLogin = function()
	{
		return function(row)
		{
			return row.employee == $rootScope.Employee || row.employee == 2;
		}
	}
  
})

.controller('VideosCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce) {

	$scope.Videos = $rootScope.MainDataArray.videos;
	
	
	for(var i=0; i < $scope.Videos.length; i++)
	{
		$scope.Videos[i].VideoLink = $sce.trustAsResourceUrl($scope.Videos[i].VideoLink);
	}
	
	
	console.log($scope.Videos);
	
})

.controller('FeedbackCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer) {

	$scope.Questions = new Array();
	$scope.RateArray = new Array();
	
	$scope.Empty = 'img/s2.png';
	$scope.Full = 'img/s1.png';
	
	
	$scope.openQuestion = 
	{
		"openquestion" : ''
	}
	
	
	
	$scope.getQuestions  = function()
	{
		$scope.Questions = $rootScope.MainDataArray.questions;

		for(i=0;i<$scope.Questions.length;i++)
		{
			$scope.Rate = new Object();
			$scope.Rate =
			{
				"rate1" : $scope.Empty,
				"rate2" : $scope.Empty,
				"rate3" : $scope.Empty,
				"rate4" : $scope.Empty,
				"rate5" : $scope.Empty,
				"val" : 0
			}

			$scope.RateArray[i] = $scope.Rate;
		}		
	}


	$scope.getQuestions();
		

	
	$scope.sendFeedback  = function() 
	{ 
		$scope.Answers = '';
		$scope.NotAnswerQuestion = 0;
		
		for(i=0;i<$scope.RateArray.length;i++)
		{
			if($scope.RateArray[i]["val"] != 0)
			$scope.Answers += String($scope.RateArray[i]["val"]+',')
			else
			$scope.NotAnswerQuestion = i+1;
		}
		console.log($scope.Answers + " : " + $scope.openQuestion.openquestion)
		

		if($scope.NotAnswerQuestion == 0)
		{
			
		    $scope.sendparams = 
			{
				"answers" : $scope.Answers,
				"openquestion" : $scope.openQuestion.openquestion 
			};
			
			SendPostRequestServer.run($scope.sendparams,$rootScope.LaravelHost+'/SendFeedback').then(function(data) {

				$scope.openQuestion.openquestion = '';
				$scope.Answers = ''
				
				$ionicPopup.alert({
				 title: 'תודה רבה, פרטי המשוב התקבלו בהצלחה, הינך מעובר לעמוד הראשי',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
				});
   
				window.location.href = "#/app/main";
				
			
			});		
		}
		else
		{
			
			$ionicPopup.alert({
			 title: "לא דירגת את שאלה מספר " + $scope.NotAnswerQuestion,
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});	
		}
	}
	

	
	 
	 $scope.starClick = function (nm1,nm2)
	 {
		 for(i=1;i<=nm1;i++)
		 {
			 console.log("Full : " + i)
			 $scope.RateArray[nm2]["rate"+i] = $scope.Full;
		 }
		 
		 for(i=5; i>nm1;i--)
		 {
			console.log("Empty : " + i)
			$scope.RateArray[nm2]["rate"+i] = $scope.Empty; 
		 }
		 
		 $scope.RateArray[nm2]["val"] = nm1;
		 console.log($scope.RateArray)
		 console.log($scope.RateArray[nm2]["val"] + " : " + $scope.nm1)
	 }

})

.controller('ArticleCategoriesCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer) {

	$scope.phpHost = $rootScope.PHPHost;
	$scope.ArticleCategories = $rootScope.MainDataArray.articles;
	console.log("articles: " , $scope.ArticleCategories)


	$scope.byBlogLogin = function()
	{
		return function(row)
		{
			return row.employee == $rootScope.Employee;
		}
	}
	
		

})

.controller('CategoryPostsCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer) {

	
	$scope.phpHost = $rootScope.PHPHost;
	$scope.CategoryIndex = $stateParams.CategoryId;
	$scope.CategoryPosts = $rootScope.MainDataArray.articles[$scope.CategoryIndex].articles;
	$scope.colorArray = new Array("#377fba","#00ac36");	
	console.log("category posts:" , $scope.CategoryPosts)

})

.controller('ArticleCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer) {

	
	$scope.phpHost = $rootScope.PHPHost;
	$scope.CategoryIndex = $stateParams.CategoryId;
	$scope.ArticleIndex = $stateParams.IndexId;
	$scope.ArticleData = $rootScope.MainDataArray.articles[$scope.CategoryIndex].articles[$scope.ArticleIndex];

})

.controller('ContactCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer) {

	$scope.contactfields = 
	{
		"name" : "",
		"phone" : "",
		"mail" : "",
		"desc" : ""
	}
	
	$scope.sendContact = function()
	{
		if ($scope.contactfields.name =="")
		{
			$ionicPopup.alert({
			 title: "יש להזין שם מלא",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});			
		}
		else if ($scope.contactfields.phone =="")
		{
			$ionicPopup.alert({
			 title: "יש להזין מספר טלפון",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});			
		}
		else if ($scope.contactfields.desc =="")
		{
			$ionicPopup.alert({
			 title: "יש להזין תוכן הפנייה",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});			
		}
		else
		{

			SendPostRequestServer.run($scope.contactfields,$rootScope.LaravelHost+'/SendContact').then(function(data) 
			{

				$ionicPopup.alert({
				 title: "תודה, פניתך התקבלה בהצלחה, נחזור אליך בהקדם",
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
				});	

				
				$scope.contactfields.name = '';
				$scope.contactfields.phone = '';
				$scope.contactfields.mail = '';
				$scope.contactfields.desc = '';
				
				window.location.href = "#/app/main";
				
			
			});	

			
		}
	}

})

.controller('UsefulCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer) {

	  $scope.Usefuldata = $rootScope.MainDataArray.useful_information;
	  console.log($scope.Usefuldata);
})

.controller('EmployeeLoginCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$ionicHistory) {

	$scope.loginfields = 
	{
		"mail" : "",
		"password" : ""
	}
	
	$scope.LoginEmployee = function()
	{
		var emailRegex = /\S+@\S+\.\S+/;
		
		if ($scope.loginfields.mail =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין דוא"ל',
			 template: '',
			 buttons: [{
			 text: 'אישור',
			 type: 'button-positive',
			  }]			 
		   });				
		}
		else if (emailRegex.test($scope.loginfields.mail) == false)
		{

			$ionicPopup.alert({
			title: 'דוא"ל לא תקין יש לתקן',
			template: '',		
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.loginfields.mail =  '';
		}
		else if ($scope.loginfields.password =="")
		{
			$ionicPopup.alert({
			 title: "יש להזין סיסמה",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});			
		}
		else
		{
		    $scope.sendparams = 
			{
				"mail" : $scope.loginfields.mail,
				"password" : $scope.loginfields.password,
				"push_id" : $rootScope.pushId
			};
			SendPostRequestServer.run($scope.sendparams,$rootScope.JCIHostURL+'/ValidateClientPassword').then(function(data) {
				
				
				
				if (data[0].status == 0)
				{
					$ionicPopup.alert({
					title: 'סיסמה שגויה יש לנסות שוב',
					template: '',		
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });	

				   $scope.loginfields.password =  '';					
				}
				else
				{
					
					$scope.loginfields.mail =  '';		
					$scope.loginfields.password =  '';	
					//localStorage.setItem('userid',data[0].userid)
					window.localStorage.employeeid = data[0].userid;
					//$localStorage.userid = data[0].userid;
					$scope.checkLoggedIn();
					window.location.href = "#/app/employeemain";	
				}
			});
			
		}
	}

})

.controller('EmployeeMainCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer) {


  $scope.$on('$ionicView.enter', function(e) {
	  $rootScope.Employee  = 1;
  });
  
  $scope.openSite = function(URL)
  {
	  iabRef = window.open(URL, '_blank', 'location=yes','toolbar=no');

  }

})

.controller('ReportIncidentCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera) {

	$scope.phpHost = $rootScope.PHPHost;
	$scope.defaultImage = 'img/upload_image.png';
	
	$scope.reportfields = 
	{
		"title" : "",
		"place" : "",
		"desc" : "",
		"image" : "",
		"host" : $rootScope.PHPHost
		
	}
	

	
	
	$scope.sendReport = function()
	{
		if ($scope.reportfields.title == "")
		{
			$ionicPopup.alert({
			 title: "יש להזין תיאור האירוע",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});				
		}
		else if ($scope.reportfields.place == "")
		{
			$ionicPopup.alert({
			 title: "יש להזין מקום האירוע",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});				
		}
		else
		{
			
			
			SendPostRequestServer.run($scope.reportfields,$rootScope.LaravelHost+'/sendIncident').then(function(data) 
			{


				$scope.reportfields.title = '';
				$scope.reportfields.place = '';
				$scope.reportfields.desc = '';
				$scope.reportfields.image = '';
				$scope.defaultImage = 'img/upload_image.png';
		
				$ionicPopup.alert({
				 title: "תודה, דיווח נשלח בהצלחה",
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
				});	
				
				window.location.href = "#/app/employeemain";					
			
			});	

		}
	}
	
	$scope.CameraOptions = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: "בחירת אפשרות:",
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: "מצלמה",
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: "גלריית תמונות",
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   	   {
		text: "ביטול",
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });		
	}

	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.chunkedMode = false;
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				
				if (data.response)
				{
					$scope.defaultImage = $rootScope.PHPHost+data.response;
					$scope.reportfields.image = data.response;
				}
				
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }	
	

})

.controller('TradingCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,SendGetRequestServer,$timeout,$cordovaCamera) {

	$scope.phpHost = $rootScope.PHPHost;
	
	$scope.getTrading = function()
	{
		SendGetRequestServer.run($rootScope.LaravelHost+'/GetTrading').then(function(data) 
		{
			$rootScope.TradingArray = data;
			console.log ("trading:" , data);
		
		});	
	}
	
	$scope.getTrading();

})

.controller('ManageTradingCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera) {

	$scope.phpHost = $rootScope.PHPHost;
	$scope.ItemId = $stateParams.ItemId;	
	$scope.defaultImage = 'img/upload_image.png';	
	
	$scope.tradingfields = 
	{
		"id" : "",
		"title" : "",
		"price" : "",
		"phone" : "",
		"desc" : "",
		"image" : "",
		"host" : $scope.phpHost
	}
	

	
	if ($scope.ItemId == "-1") 
	{
		$scope.updateUrl = "AddTrading";
		$scope.updateText = "נוספה";
	}
		
	else
	{
		$scope.updateUrl = "UpdateTrading";
		$scope.updateText = "עודכנה";
		$scope.tradingData = $rootScope.TradingArray[$scope.ItemId];
		$scope.tradingfields.title = $scope.tradingData.prd_name;
		$scope.tradingfields.price = $scope.tradingData.price;
		$scope.tradingfields.phone = $scope.tradingData.phone;
		$scope.tradingfields.desc = $scope.tradingData.desc;
		$scope.tradingfields.id = $scope.tradingData.index;
		
		if ($scope.tradingData.image)
			$scope.tradingfields.image = $scope.tradingData.image;
		
	}
		
		

	
	$scope.sendTrading = function()
	{
		if ($scope.tradingfields.title =="")
		{
			$ionicPopup.alert({
			 title: "יש להזין שם המוצר",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});				
		}
		else if ($scope.tradingfields.price =="")
		{
			$ionicPopup.alert({
			 title: "יש להזין מחיר",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});				
		}
		else if ($scope.tradingfields.phone =="")
		{
			$ionicPopup.alert({
			 title: "יש להזין טלפון ליצירת קשר",
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});				
		}
		else
		{

			SendPostRequestServer.run($scope.tradingfields,$rootScope.LaravelHost+'/'+$scope.updateUrl).then(function(data) 
			{


				$scope.tradingfields.title = '';
				$scope.tradingfields.price = '';
				$scope.tradingfields.phone = '';
				$scope.tradingfields.desc = '';
				$scope.tradingfields.image = '';
				$scope.defaultImage = 'img/upload_image.png';
		
				$ionicPopup.alert({
				 title: "מודעה "+$scope.updateText+" בהצלחה",
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
				});	
				
				window.location.href = "#/app/trading";					
			
			});		
		}
		
	}
	
	

	$scope.CameraOptions = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: "בחירת אפשרות:",
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: "מצלמה",
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: "גלריית תמונות",
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   	   {
		text: "ביטול",
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });		
	}

	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.chunkedMode = false;
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				
				if (data.response)
				{
					$scope.defaultImage = $rootScope.PHPHost+data.response;
					//$scope.tradingfields.image = data.response;
				}
				
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }		
	
	
	
	

})

.controller('JciMainCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera) {


	$scope.mainCategories = [];
	$scope.hostUrl = $rootScope.JCIHostURL;
	
	$scope.date = new Date("05/23/2017 9:00:00"); 
	$scope.countdownseconds = $scope.date.getTime();
	

	$rootScope.$watch('CategoryArray', function(newValue, oldValue) {
      $scope.mainCategories = $rootScope.CategoryArray.categories;
    });

})

.controller('JciDetailsCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera,$ionicLoading) {

	$scope.hostUrl = $rootScope.JCIHostURL;
	$scope.itemId = $stateParams.ItemId;
	$scope.InfoArray = $rootScope.CategoryArray.categories[$scope.itemId];
	//$scope.navTitle = $scope.InfoArray.title;
	//console.log("Details: " , $scope.InfoArray);

	$ionicLoading.show({
	  template: '<ion-spinner icon="lines" class="spinner-assertive"></ion-spinner>',
	  noBackdrop : false,
	  duration : 10000
	});
	

  $scope.$on('imageloaded', function(events, args){
	$ionicLoading.hide();
  })	

})

.controller('JciPhoneBookCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera,$ionicLoading) {

	$scope.hostUrl = $rootScope.JCIHostURL;
	$scope.PhonebookArray = $rootScope.CategoryArray.phonebook;
	
	
	$scope.DialPhone = function(phone)
	{
		window.open('tel:' + phone, '_system');
	}
	
	$scope.BlurContacts = function()
	{
		$ionicScrollDelegate.scrollTop();
	}	
	
	
	$scope.CutNumber = function(number)
	{
		if (number)
		{
			$scope.SubPhone = number.substring(0, 6);
			if ($scope.SubPhone == "050843")
			{
				$scope.NewString = number.substring(6);
				return $scope.NewString;				
			}
			else
				return number;	
		}
	}
	
	

})

.controller('JciPushMessagesCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera,$ionicLoading) {


	$scope.hostUrl = $rootScope.HostURL;
	$scope.pushMessage = $rootScope.pushContent;

	$rootScope.$on('pushmessage', function(event, args) 
	{
		
		 $timeout(function() 
		 {
			$scope.pushMessage = String(args);
		 }, 300);
	});

	
	
	$rootScope.$watch('pushContent', function(newValue, oldValue) {
		$scope.pushMessage = $rootScope.pushContent;
    });  

	
})



.controller('JciPushMessagesCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera,$ionicLoading) {
	
	$scope.hostUrl = $rootScope.JCIHostURL;
	$scope.mainMessages = [];
	
	/*
	$rootScope.$watch('CategoryArray', function(newValue, oldValue) {
      $scope.mainMessages = $rootScope.CategoryArray.push_messages;
    }); 	
	*/
	
	$scope.getUserNotifications = function()
	{
		$scope.sendparams = 
		{
			"user" : window.localStorage.employeeid,//$localStorage.userid,
			"push_id" : $rootScope.pushId, //"4d215ebd-f48c-43ad-a778-559415a72997"
		};
		SendPostRequestServer.run($scope.sendparams,$rootScope.JCIHostURL+'/GetUserPushMessages').then(function(data) {
			
			$scope.mainMessages = data;
			console.log("push msgs: " , data)

		});	
	}
	
	$scope.getUserNotifications();
	
	
	
	$rootScope.$on('pushmessage', function(event, args) 
	{
		
		 $timeout(function() 
		 {
			var date = new Date()
			var hours = date.getHours()
			var minutes = date.getMinutes()
		
			if (hours < 10)
			hours = " " + hours
			
			if (minutes < 10)
			minutes = "0" + minutes
		
			var time = hours+':'+minutes;

			$scope.mainMessages.unshift({
				"content": String(args),
				"date": "01/0/01 "+time
			});	 
			
			//$scope.mainMessages.reverse();
			
		 }, 300);	
	});


	
	$scope.GetTime = function(date)
	{
		$scope.splitDate = date.split(" ");
		$scope.SplitTime = $scope.splitDate[1].split(":");
		$scope.newTime = $scope.SplitTime[0]+':'+$scope.SplitTime[1];
		return $scope.newTime;
	}	
	
	

})

.controller('JciPushMsgCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera,$ionicLoading) {


	$scope.hostUrl = $rootScope.JCIHostURL;
	$scope.pushMessage = $rootScope.pushContent;

	$rootScope.$on('pushmessage', function(event, args) 
	{
		
		 $timeout(function() 
		 {
			$scope.pushMessage = String(args);
		 }, 300);
	});

	
	
	$rootScope.$watch('pushContent', function(newValue, oldValue) {
		$scope.pushMessage = $rootScope.pushContent;
    });  

	
	
})

.controller('HowArriveCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera,$ionicLoading) {


})

.controller('NavigationCtrl', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera,$ionicLoading) {
	
	
	$scope.navigationInput = '';
	$scope.NavigationId = '';
	$scope.NavigationArray = [];
	
	$scope.FocusField = function()
	{
		// $ionicScrollDelegate.scrollTop();
		//$scope.$broadcast('scroll.scrollTop');
		//$anchorScroll();
		//  $window.scrollTo(0,0);
	}
	
	$scope.getData = function(index)
	{			
		if (index == 1)
		{
			$scope.ShowDevision = true;
			$scope.ShowBuildings = false;
			$scope.SearchByName = "חפש מחלקה";
			$scope.NavigationType = 0;
			$scope.NavigationArray = $rootScope.MainDataArray.navigation;
			$ionicScrollDelegate.scrollTop();
		}
		else 
		{
			$scope.ShowDevision = false;
			$scope.ShowBuildings = true;
			$scope.SearchByName = "חפש בניין";
			$scope.NavigationType = 1;
			$scope.NavigationArray = $rootScope.MainDataArray.buildings;
			$ionicScrollDelegate.scrollTop();
		}
		
	}
	
	
	$scope.getData(1);
	
	
	$scope.navigateUrl = function(index,buildingnumber)
	{
		if ($scope.NavigationType == 0)
			window.location.href = "#/app/navigation_info/"+index;
		else
			window.location.href = "#/app/buildings/"+buildingnumber;
		
	}
	
	$scope.BlurNavigation = function()
	{
		$ionicScrollDelegate.scrollTop();
	}
	


})

.controller('NavigationBuilding', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera,$ionicLoading) {
	
	
	
	$scope.GetBuilding = function()
	{
		$scope.sendparams = 
		{
			"building_number" : $stateParams.ItemId
		};
		SendPostRequestServer.run($scope.sendparams,$rootScope.LaravelHost+'/GetBuildingInfo').then(function(data) {
			
			$scope.buildingData = data;
			console.log("buildingData" , $scope.buildingData)

		});			
	}
	
	$scope.GetBuilding();


			
	$scope.BlurBuildings = function()
	{
		$ionicScrollDelegate.scrollTop();
	}
		$scope.navigateUrl = function (Path,Num) 
	{
		window.location.href = Path+String(Num);
	}


})	


.controller('NavigationInfo', function($scope, $stateParams,$rootScope,$ionicScrollDelegate,$sce,$ionicPopup,SendPostRequestServer,$timeout,$cordovaCamera,$ionicLoading) {
	
	
	$scope.NavigationInfo = "";
	
	for(i=0;i<$rootScope.MainDataArray.navigation.length;i++)
	{
		if ($rootScope.MainDataArray.navigation[i].index == $stateParams.ItemId)
		{
			$scope.NavigationInfo = $rootScope.MainDataArray.navigation[i];
		}				
	}
	
	
	$scope.buildingname = $scope.NavigationInfo.building_name;
	$scope.devision = $scope.NavigationInfo.devision;
	$scope.floor = $scope.NavigationInfo.floor;
	$scope.area = $scope.NavigationInfo.area;
	$scope.how_to_arrive = $scope.NavigationInfo.how_to_arrive;
	$scope.mapimage = $scope.NavigationInfo.building_name_signing;	
	
})	

.directive('imageonload', function($ionicLoading,$rootScope) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                //alert('image is loaded');
				$rootScope.$broadcast('imageloaded')
            });
        }
    };
})

.filter('cutString_TitlePage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' ');
    };
})

.filter('cutString_ListPage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' - המשך לכתבה');
    };
})

.filter('toTrusted', function ($sce) 
{
    return function (value) {
        return $sce.trustAsHtml(value);
    };
})

.filter('formatDate', function () {
    return function(text){
	dateArray = text.split(' ');
	return dateArray[0];

    }
})

