// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.factories','ngCordova','ionic.ion.imageCacheFactory','timer'])



.run(function($ionicPlatform,$rootScope,SendGetRequestServer,SendPostRequestServer) {
	
	
   $rootScope.LaravelHost = "http://zivhospital.tapper.co.il/laravel/public/";
   $rootScope.PHPHost = "http://zivhospital.tapper.co.il/php/";
   $rootScope.JCIHostURL = 'http://ziv.tapper.co.il/';
   $rootScope.Employee  = 0;	
   $rootScope.TradingArray  = [];
   $rootScope.CategoryArray = [];
	
  $ionicPlatform.ready(function() {
	  
	  
	  $rootScope.getMainData = function()
	  {

			SendGetRequestServer.run($rootScope.LaravelHost+'/getMainData').then(function(data) {
				$rootScope.MainDataArray = data;
				//alert (JSON.stringify(data));
				console.log(data);
			});		
	  }
	  
	  $rootScope.getMainData();	  
	  
	  
	  $rootScope.getCategories = function()
	  {
		    $rootScope.sendparams = 
			{
				"push_id" : $rootScope.pushId  // "91090585-595c-474a-96c6-babfb8b40763" // 
			};
			SendPostRequestServer.run($rootScope.sendparams,$rootScope.JCIHostURL+'/GetCategoriesClient').then(function(data) {
				$rootScope.CategoryArray = data;
				console.log(data);
			});		
	  }
		  //if (!window.cordova)
			$rootScope.getCategories();
	  
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	
	
  var notificationOpenedCallback = function(jsonData) {
	    
	  $rootScope.$broadcast('pushmessage',jsonData.message);	


		
	  //alert (jsonData.additionalData.type);
	  //$rootScope.pushContent = '';
	  $rootScope.pushContent = jsonData.message;
	  
	  if (!jsonData.isActive)
	  {
		  if (jsonData.additionalData.type == "pushmessage")
			 window.location ="#/app/jci_pushmsg";
	  }


	  
    console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
  };

  window.plugins.OneSignal.init("11e0c946-2556-4be8-a113-105cafb905e7",
                                 {googleProjectNumber: "595323574268"},
                                 notificationOpenedCallback);

								 
			

  window.plugins.OneSignal.getIds(idsReceivedCallback);
  
	function idsReceivedCallback(ids) {
		
		$rootScope.pushId = ids.userId;
		//$rootScope.getCategories();

	}

								 
  // Show an alert box if a notification comes in when the user is in your app.
  window.plugins.OneSignal.enableInAppAlertNotification(false);

  
	
	
	
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })


    .state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/about.html',
          controller: 'AboutCtrl'
        }
      }
    })

    .state('app.phonebook', {
      url: '/phonebook',
      views: {
        'menuContent': {
          templateUrl: 'templates/phonebook.html',
          controller: 'PhonebookCtrl'
        }
      }
    })
	
    .state('app.videos', {
      url: '/videos',
      views: {
        'menuContent': {
          templateUrl: 'templates/videos.html',
          controller: 'VideosCtrl'
        }
      }
    })	

    .state('app.feedback', {
      url: '/feedback',
      views: {
        'menuContent': {
          templateUrl: 'templates/feedback.html',
          controller: 'FeedbackCtrl'
        }
      }
    })		

    .state('app.articlecategories', {
      url: '/articlecategories',
      views: {
        'menuContent': {
          templateUrl: 'templates/article_categories.html',
          controller: 'ArticleCategoriesCtrl'
        }
      }
    })

    .state('app.categoryposts', {
      url: '/categoryposts/:CategoryId',
      views: {
        'menuContent': {
          templateUrl: 'templates/categoryposts.html',
          controller: 'CategoryPostsCtrl'
        }
      }
    })	
	
    .state('app.article', {
      url: '/article/:CategoryId/:IndexId',
      views: {
        'menuContent': {
          templateUrl: 'templates/article.html',
          controller: 'ArticleCtrl'
        }
      }
    })

    .state('app.contact', {
      url: '/contact',
      views: {
        'menuContent': {
          templateUrl: 'templates/contact.html',
          controller: 'ContactCtrl'
        }
      }
    })

    .state('app.useful', {
      url: '/useful',
      views: {
        'menuContent': {
          templateUrl: 'templates/useful.html',
          controller: 'UsefulCtrl'
        }
      }
    })	

    .state('app.employeelogin', {
      url: '/employeelogin',
      views: {
        'menuContent': {
          templateUrl: 'templates/employeelogin.html',
          controller: 'EmployeeLoginCtrl'
        }
      }
    })	

    .state('app.employeemain', {
      url: '/employeemain',
      views: {
        'menuContent': {
          templateUrl: 'templates/employeemain.html',
          controller: 'EmployeeMainCtrl'
        }
      }
    })	

    .state('app.reportincident', {
      url: '/reportincident',
      views: {
        'menuContent': {
          templateUrl: 'templates/reportincident.html',
          controller: 'ReportIncidentCtrl'
        }
      }
    })		

    .state('app.trading', {
      url: '/trading',
      views: {
        'menuContent': {
          templateUrl: 'templates/trading.html',
          controller: 'TradingCtrl'
        }
      }
    })		

    .state('app.managetrading', {
      url: '/managetrading/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/managetrading.html',
          controller: 'ManageTradingCtrl'
        }
      }
    })	

    .state('app.how_arrive', {
      url: '/how_arrive',
      views: {
        'menuContent': {
          templateUrl: 'templates/how_arrive.html',
          controller: 'HowArriveCtrl'
        }
      }
    })	

    .state('app.navigation', {
      url: '/navigation',
      views: {
        'menuContent': {
          templateUrl: 'templates/navigation.html',
          controller: 'NavigationCtrl'
        }
      }
    })		
	

     .state('app.buildings', {
    url: '/buildings/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/building.html',
        controller: 'NavigationBuilding'
      }
    }
  })
  
     .state('app.navigation_info', {
    url: '/navigation_info/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/navigation_info.html',
        controller: 'NavigationInfo'
      }
    }
  })	
	
	
	/* JCI */ 
	
    .state('app.jciapp', {
      url: '/jciapp',
      views: {
        'menuContent': {
          templateUrl: 'templates/jci-main.html',
          controller: 'JciMainCtrl'
        }
      }
    })		

    .state('app.jci_details', {
      url: '/jci_details/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/jci/details.html',
          controller: 'JciDetailsCtrl'
        }
      }
    })

    .state('app.jci_phonebook', {
      url: '/jci_phonebook',
      views: {
        'menuContent': {
          templateUrl: 'templates/jci/phonebook.html',
          controller: 'JciPhoneBookCtrl'
        }
      }
    })	

    .state('app.jci_pushmessages', {
      url: '/jci_pushmessages',
      views: {
        'menuContent': {
          templateUrl: 'templates/jci/pushmessages.html',
          controller: 'JciPushMessagesCtrl'
        }
      }
    })		

    .state('app.jci_pushmsg', {
      url: '/jci_pushmsg',
      views: {
        'menuContent': {
          templateUrl: 'templates/jci/pushmsg.html',
          controller: 'JciPushMsgCtrl'
        }
      }
    })	
	
	
	
	/* JCI END */
	
	;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});
