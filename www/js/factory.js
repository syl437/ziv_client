angular.module('starter.factories', [])

.factory('SendGetRequestServer', SendGetRequestServer)
.factory('SendPostRequestServer', SendPostRequestServer)
//.factory('ClosePopupService' , ClosePopupService)


.factory('ClosePopupService', function($document, $ionicPopup, $timeout){
        var lastPopup;
        return {
            register: function(popup) {
                $timeout(function(){
                    var element = $ionicPopup._popupStack.length>0 ? $ionicPopup._popupStack[0].element : null;
                    if(!element || !popup || !popup.close) return;
                    element = element && element.children ? angular.element(element.children()[0]) : null;
                    lastPopup  = popup;
                    var insideClickHandler = function(event){
                        event.stopPropagation();
                    };
                    var outsideHandler = function() {
                        popup.close();
                    };
                    element.on('click', insideClickHandler);
                    $document.on('click', outsideHandler);
                    popup.then(function(){
                        lastPopup = null;
                        element.off('click', insideClickHandler);
                        $document.off('click', outsideHandler);
                    });
                });
            },
            closeActivePopup: function(){
                if(lastPopup) {
                    $timeout(lastPopup.close);
                    return lastPopup;
                }
            }
        };
    })
	
	

function SendGetRequestServer($http,$rootScope,$ionicLoading, $q) 
{
	
  return {
	  run: function (url) {
		  
		  var deferred = $q.defer();
		  //console.log(params)
		  
        $ionicLoading.show({
          template: '<ion-spinner icon="lines" class="spinner-assertive"></ion-spinner>',
          noBackdrop : false,
          duration : 10000
        });

				
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';	
			$http.get(url)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				// return data;
				deferred.resolve(data);
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				// return data;
				deferred.reject(data);
			});
			
			return deferred.promise;
	  }
  }
}



function SendPostRequestServer($http,$rootScope,$ionicLoading, $q) 
{
	
  return {
	  run: function (params,url) {

		  var deferred = $q.defer();
		  //console.log(params)
		  
        $ionicLoading.show({
          template: '<ion-spinner icon="lines" class="spinner-assertive"></ion-spinner>',
          noBackdrop : false,
          duration : 10000
        });

				
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';	
			$http.post(url,params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				// return data;
				deferred.resolve(data);
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				// return data;
				deferred.reject(data);
			});
			
			return deferred.promise;
	  }
  }
}


 









